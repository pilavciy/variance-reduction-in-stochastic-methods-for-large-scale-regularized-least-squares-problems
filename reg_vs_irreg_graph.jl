using KrylovKit
using RandomForests
using LightGraphs
using PyPlot
using LinearAlgebra
using Random
using GraphPlot
using JLD2
using SparseArrays
using StatsBase
using LaTeXStrings
import StatsBase.denserank,Statistics.mean,Base.show,Base.sum,
StatsBase.counts,StatsBase.std

pygui(false)
include("./utils.jl")

##  Generate graphs
n = 1000
k = Int64(round(n*0.01))
G_reg = LightGraphs.random_regular_graph(n,2*k)
@assert (is_connected(G_reg))
display("nv=$(nv(G_reg)), ne=$(ne(G_reg))")
G_ir = LightGraphs.barabasi_albert(n,k)
@assert (is_connected(G_ir))
display("nv=$(nv(G_ir)), ne=$(ne(G_ir))")
## Generate a signal
y = randn(n)

## Experiment setup
q = 1.0
xhat_reg = (laplacian_matrix(G_reg) ./ q+ I ) \ y
xhat_ir = (laplacian_matrix(G_ir) ./ q+ I ) \ y

ALPHAS = range(0.0,stop=(2*q/(q+2*k)),length=50)
EXPREP = 200

mse_Zbar_reg = zeros(EXPREP,length(ALPHAS))
mse_Zbar_ir = zeros(EXPREP,length(ALPHAS))

nrep = 10
for exprep = 1 : EXPREP
    for (aidx,α) in enumerate(ALPHAS)
        zbar_reg = smooth_rf_CV(G_reg,q,y,α;nrep=nrep)
        mse_Zbar_reg[exprep,aidx] = norm(zbar_reg - xhat_reg)^2
        zbar_ir = smooth_rf_CV(G_ir,q,y,α;nrep=nrep)
        mse_Zbar_ir[exprep,aidx] = norm(zbar_ir - xhat_ir)^2
    end
end
_,α_hat_reg = smooth_rf_CV(G_reg,q,y;nrep=nrep)
_,α_hat_ir = smooth_rf_CV(G_ir,q,y;nrep=nrep)

## PLOTS

f=figure(figsize= [10,4])

PyPlot.subplot(1,2,1)
plot(ALPHAS,mean(mse_Zbar_reg,dims=1)[:])
plot(ALPHAS,repeat([[mean(mse_Zbar_reg,dims=1)[1]]],length(ALPHAS)))
axvline(x=(2*q/(q+2*maximum(degree(G_reg)))),color="green",linestyle="--",linewidth=2)
axvline(x=α_hat_reg,linestyle="dashdot",color="red",linewidth=2)
scatter(x=ALPHAS[argmin(mean(mse_Zbar_reg,dims=1)[:])],y=minimum(mean(mse_Zbar_reg,dims=1)))
xlabel("α",fontsize=18)
ylabel("Squared error",fontsize=18)
title("Regular Graph",fontsize=18)
PyPlot.xticks(fontsize=15)
PyPlot.yticks(fontsize=15)


PyPlot.subplot(1,2,2)
plot(ALPHAS,mean(mse_Zbar_ir,dims=1)[:],label=latexstring("\$||\\bar{z}\$-\$\\hat{x}||^2_2\$"))
plot(ALPHAS,repeat([mean(mse_Zbar_ir,dims=1)[1]],length(ALPHAS)),label=latexstring("\$||\\bar{x}\$-\$\\hat{x}||^2_2\$"))
axvline(x=(2*q/(q+2*maximum(degree(G_ir)))),linestyle="--",color="green",linewidth=2,label=latexstring("\$\\alpha=\\frac{2q}{q+2d_{max}}\$"))
axvline(x=α_hat_ir,linestyle="dashdot",color="red",linewidth=2,label=latexstring("\$\\hat{\\alpha}\$"))
scatter(x=ALPHAS[argmin(mean(mse_Zbar_ir,dims=1)[:])],y=minimum(mean(mse_Zbar_ir,dims=1)),label="Min. error")

xlabel("α",fontsize=18)
# ylabel("Squared error",fontsize=18)
title("Barabasi-Albert Graph",fontsize=18)
PyPlot.xticks(fontsize=15)
PyPlot.yticks(fontsize=15)

figlegend(loc="upper center",ncol = 5,fontsize=14, bbox_to_anchor=(0.525, 0.125),framealpha=1.0)
tight_layout(rect=[0, 0.06, 1, 1.0])
savefig("./results/reg_vs_ir_alpha.pdf")
