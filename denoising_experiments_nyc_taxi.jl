using KrylovKit
using RandomForests
using LightGraphs,SimpleWeightedGraphs
using PyPlot,Plots
using LinearAlgebra
using Random
using GraphPlot
using JLD2
using SparseArrays
using StatsBase
using NearestNeighbors
using LaTeXStrings
using Statistics
using PolygonOps
using Plots,Shapefile,PolygonOps,DataFrames
import StatsBase.denserank,Statistics.mean,Base.show,Base.sum,
StatsBase.counts,StatsBase.std,StatsBase.psnr
using MAT
using Images
using Plots.PlotMeasures

pygui(true)
include("./ssl_data_loaders.jl")
include("./utils.jl")
PyPlot.close("all")
## Choose a graph
gname = "taxi"
if gname == "taxi"
    JLD2.@load "./data/nyc-taxi/graph-signal.jld" G signal
    JLD2.@load "./data/nyc-taxi/data.jld" data_df
else
    display("Not found")
    @assert(0)
end

S = signal
L = laplacian_matrix(G)
n = nv(G)
@assert is_connected(G)

display("Graph:$gname,nvert:$(nv(G)),nedges:$(ne(G))")

# Experiment params.
σ = 25.0
Q = 10 .^ (-1.0 :0.5:1.0)
NREP_arr = [2 5 7 100]
EXPREP = 50
d = degree(G)
alphas(q) = [2*q/(q+2*maximum(degree(G)))]

err_xbar = zeros(length(NREP_arr),EXPREP,length(Q))
err_zbar_alpha = zeros(length(NREP_arr),EXPREP,length(Q),2)
err_zbar_alpha_hat = zeros(length(NREP_arr),EXPREP,length(Q))
err_zbar_alpha_hat_prec = zeros(length(NREP_arr),EXPREP,length(Q))

PSNR_noise = zeros(EXPREP)
PSNR_org = zeros(EXPREP,length(Q))
PSNR_xbar = zeros(length(NREP_arr),EXPREP,length(Q))
PSNR_zbar_alpha = zeros(length(NREP_arr),EXPREP,length(Q),2)
PSNR_zbar_alpha_hat = zeros(length(NREP_arr),EXPREP,length(Q))
PSNR_zbar_alpha_hat_prec = zeros(length(NREP_arr),EXPREP,length(Q))

## Create noisy signal
S_noisy = S + randn(size(S))*σ

Plots.plot(data_df.geometry,fill_z = S',fill=cgrad(:vikO),xaxis=false,yaxis=false,xticks=false,yticks=false,colorbar=true,size=(300,300),clims=(0,120))
Plots.savefig("./results/taxi2/original_signal")
Plots.plot(data_df.geometry,fill_z = S_noisy',fill=cgrad(:vikO),xaxis=false,yaxis=false,xticks=false,yticks=false,colorbar=true,size=(300,300),clims=(0,120))
Plots.savefig("./results/taxi2/noisy_signal")
for exp_rep = 1 : EXPREP
    S_noisy = S + randn(size(S))*σ
    PSNR_noise[exp_rep] =  Images.assess_psnr(S_noisy,S,maximum(S))

    for (qidx,q) in enumerate(Q)
        P = Diagonal((degree(G)[:]/q .+ 1) .^ (-1))
        ## Calculate the direct solution
        exact = Matrix(L./q + I)\S_noisy
        PSNR_org[exp_rep,qidx] =  Images.assess_psnr(exact,S,maximum(S))
        for (idx,nrep) in enumerate(NREP_arr)
            ## Estimate via xbar
            est_xbar = smooth_rf(G,q,S_noisy;nrep=nrep,variant=2).est
            err_xbar[idx,exp_rep,qidx] = norm(est_xbar - exact)^2
            PSNR_xbar[idx,exp_rep,qidx]  =  Images.assess_psnr(est_xbar,S,maximum(S))

            ## Estimate via zbar with fixed alpha
            for (aidx,α) in enumerate(alphas(q))
                est_alpha_c = smooth_rf_CV(G,q,S_noisy,α;nrep=nrep)
                err_zbar_alpha[idx,exp_rep,qidx,aidx] = norm(est_alpha_c - exact)^2
                PSNR_zbar_alpha[idx,exp_rep,qidx,aidx]  =   Images.assess_psnr(est_alpha_c,S,maximum(S))
            end

            ## Estimate via zbar with alpha_hat
            (est_alpha_hat,_) = smooth_rf_CV(G,q,S_noisy;nrep=nrep)
            err_zbar_alpha_hat[idx,exp_rep,qidx] = norm(est_alpha_hat - exact)^2
            PSNR_zbar_alpha_hat[idx,exp_rep,qidx]  =    Images.assess_psnr(est_alpha_hat,S,maximum(S))

            ## Estimate via preconditioned zbar with alpha_hat
            (est_alpha_hat,_) = smooth_rf_CV(G,q,S_noisy;nrep=nrep,P= P)
            err_zbar_alpha_hat_prec[idx,exp_rep,qidx] = norm(est_alpha_hat - exact)^2
            PSNR_zbar_alpha_hat_prec[idx,exp_rep,qidx]  =  Images.assess_psnr(est_alpha_hat,S,maximum(S))

            display("exp_rep:$exp_rep,q:$(round(q,sigdigits=2)),nrep:$nrep")

        end
    end
end
# Best perf xhat
q = Q[2]
est_xhat = smooth(G,q,S_noisy)
Plots.plot(data_df.geometry,fill_z = est_xhat',fill=cgrad(:vikO),xaxis=false,yaxis=false,xticks=false,yticks=false,colorbar=true,size=(300,300),clims=(0,120))
Plots.savefig("./results/taxi2/est_xhat")

## PLOT 1 NREP
est_xbar = smooth_rf(G,q,S_noisy;nrep=1,variant=2).est
Plots.plot(data_df.geometry,fill_z = est_xbar',fill=cgrad(:vikO),xaxis=false,yaxis=false,xticks=false,yticks=false,colorbar=true,size=(300,300),clims=(0,120))
Plots.savefig("./results/taxi2/xbar_1rep")

Plots.plot(data_df.geometry,fill_z = ((L./q + I)*est_xbar)',fill=cgrad(:vikO),xaxis=false,yaxis=false,xticks=false,yticks=false,colorbar=true,size=(300,300),clims=(0,120))
Plots.savefig("./results/taxi2/cv_1rep")


Plots.plot(data_df.geometry,fill_z = (est_xbar - alphas(q)[1]*(L./q + I)*est_xbar)',fill=cgrad(:vikO),xaxis=false,yaxis=false,xticks=false,yticks=false,colorbar=true,size=(300,300),clims=(0,120))
Plots.savefig("./results/taxi2/zbar_1rep_maxdeg")



outputfolder = string("./results/",gname)
JLD2.@load string(outputfolder,"2/workspace.jld2") G err_xbar err_zbar_alpha err_zbar_alpha_hat err_zbar_alpha_hat_prec PSNR_noise PSNR_org PSNR_xbar PSNR_zbar_alpha PSNR_zbar_alpha_hat PSNR_zbar_alpha_hat_prec Q NREP_arr EXPREP
pygui(true)
## PSNR vs Q
nrepidx = 1
figure(figsize=[6,6])
PyPlot.plot(Q,mean(PSNR_org,dims=1)',label=latexstring("\$\\hat{x}\$"),marker="o",linestyle="-",fillstyle="none",markersize=10,linewidth= 3,color="black")
PyPlot.plot(Q,mean(PSNR_xbar[nrepidx,:,:],dims=1)',label=latexstring("\$\\bar{x}\$"),marker="o",linestyle="--",fillstyle="none",markersize=10,linewidth= 2,color="lime")
PyPlot.plot(Q,repeat([mean(PSNR_noise)],length(Q)),linestyle="--",fillstyle="none",markersize=10,linewidth= 2,color="red",label="Noisy signal")
aidx = 1; PyPlot.plot(Q,mean(PSNR_zbar_alpha[nrepidx,:,:,aidx],dims=1)',label=latexstring("\$\\bar{z}\$"),marker="^",linestyle="-",markersize=10,linewidth= 3)
PyPlot.plot(Q,mean(PSNR_zbar_alpha_hat[nrepidx,:,:],dims=1)',label=latexstring("\$\\bar{z}\$ with \$\\hat{\\alpha}\$"),marker="o",linestyle="-",markersize=10,linewidth= 3)

yscale("log")
xscale("log")
xlabel("q",fontsize=18)
ylabel("PSNR",fontsize=18)
title("PSNR vs q",fontsize=20)
PyPlot.xticks(fontsize=18)
PyPlot.yticks(fontsize=18)

legend(fontsize=18)
tight_layout()
PyPlot.savefig("./results/taxi2/denoising_perf.pdf", bbox_inches = "tight")

nrepidx = 2
figure()
PyPlot.plot(Q,repeat([mean(PSNR_noise)],length(Q)),linestyle="--",fillstyle="none",markersize=10,linewidth= 2,color="red")
PyPlot.plot(Q,mean(PSNR_xbar[nrepidx,:,:],dims=1)',label="org NREP=$(NREP_arr[nrepidx])",marker="o",linestyle="--",fillstyle="none",markersize=10,linewidth= 4,color="lime")
aidx = 1; PyPlot.plot(Q,mean(PSNR_zbar_alpha[nrepidx,:,:,aidx],dims=1)',label=latexstring("\$\\bar{z}\$ with \$\\alpha_{d_{max}}\$, NREP=$(NREP_arr[nrepidx])"),marker="^",linestyle="-",markersize=10,linewidth= 3)
aidx += 1; PyPlot.plot(Q,mean(PSNR_zbar_alpha[nrepidx,:,:,aidx],dims=1)',label=latexstring("\$\\bar{z}\$ with \$\\alpha_{d_{mean}}\$, NREP=$(NREP_arr[nrepidx])"),marker="v",linestyle="-",markersize=10,linewidth= 3)
PyPlot.plot(Q,mean(PSNR_zbar_alpha_hat[nrepidx,:,:],dims=1)',label=latexstring("\$\\bar{z}\$ with \$\\hat{\\alpha}\$, NREP=$(NREP_arr[nrepidx])"),marker="o",linestyle="-",markersize=10,linewidth= 3,color="darkgreen")
PyPlot.plot(Q,mean(PSNR_org,dims=1)',label="org NREP=$(NREP_arr[nrepidx])",marker="o",linestyle="-",fillstyle="none",markersize=10,linewidth= 4,color="black")
PyPlot.plot(Q,mean(PSNR_zbar_alpha_hat_prec[nrepidx,:,:],dims=1)',label=latexstring("\$\\bar{z}\$ with \$\\hat{\\alpha}\$ prec, NREP=$(NREP_arr[nrepidx])"),marker="o",linestyle="-",markersize=10,linewidth= 3,color="darkgreen")

xscale("log")
yscale("log")

xlabel("q")
ylabel("PSNR")
title("PSNR vs q")
legend()

nrepidx = 3
figure()
PyPlot.plot(Q,repeat([mean(PSNR_noise)],length(Q)),linestyle="--",fillstyle="none",markersize=10,linewidth= 2,color="red")
PyPlot.plot(Q,mean(PSNR_xbar[nrepidx,:,:],dims=1)',label="org NREP=$(NREP_arr[nrepidx])",marker="o",linestyle="--",fillstyle="none",markersize=10,linewidth= 4,color="lime")
aidx = 1; PyPlot.plot(Q,mean(PSNR_zbar_alpha[nrepidx,:,:,aidx],dims=1)',label=latexstring("\$\\bar{z}\$ with \$\\alpha_{d_{max}}\$, NREP=$(NREP_arr[nrepidx])"),marker="^",linestyle="-",markersize=10,linewidth= 3)
aidx += 1; PyPlot.plot(Q,mean(PSNR_zbar_alpha[nrepidx,:,:,aidx],dims=1)',label=latexstring("\$\\bar{z}\$ with \$\\alpha_{d_{mean}}\$, NREP=$(NREP_arr[nrepidx])"),marker="v",linestyle="-",markersize=10,linewidth= 3)
PyPlot.plot(Q,mean(PSNR_zbar_alpha_hat[nrepidx,:,:],dims=1)',label=latexstring("\$\\bar{z}\$ with \$\\hat{\\alpha}\$, NREP=$(NREP_arr[nrepidx])"),marker="o",linestyle="-",markersize=10,linewidth= 3,color="darkgreen")
PyPlot.plot(Q,mean(PSNR_org,dims=1)',label="org NREP=$(NREP_arr[nrepidx])",marker="o",linestyle="-",fillstyle="none",markersize=10,linewidth= 4,color="black")
PyPlot.plot(Q,mean(PSNR_zbar_alpha_hat_prec[nrepidx,:,:],dims=1)',label=latexstring("\$\\bar{z}\$ with \$\\hat{\\alpha}\$ prec, NREP=$(NREP_arr[nrepidx])"),marker="o",linestyle="-",markersize=10,linewidth= 3,color="darkgreen")

xscale("log")
yscale("log")

xlabel("q")
ylabel("PSNR")
title("PSNR vs q")
legend()


nrepidx = 4
figure()
PyPlot.plot(Q,repeat([mean(PSNR_noise)],length(Q)),linestyle="--",fillstyle="none",markersize=10,linewidth= 2,color="red")
PyPlot.plot(Q,mean(PSNR_xbar[nrepidx,:,:],dims=1)',label="org NREP=$(NREP_arr[nrepidx])",marker="o",linestyle="--",fillstyle="none",markersize=10,linewidth= 4,color="lime")
aidx = 1; PyPlot.plot(Q,mean(PSNR_zbar_alpha[nrepidx,:,:,aidx],dims=1)',label=latexstring("\$\\bar{z}\$ with \$\\alpha_{d_{max}}\$, NREP=$(NREP_arr[nrepidx])"),marker="^",linestyle="-",markersize=10,linewidth= 3)
aidx += 1; PyPlot.plot(Q,mean(PSNR_zbar_alpha[nrepidx,:,:,aidx],dims=1)',label=latexstring("\$\\bar{z}\$ with \$\\alpha_{d_{mean}}\$, NREP=$(NREP_arr[nrepidx])"),marker="v",linestyle="-",markersize=10,linewidth= 3)
PyPlot.plot(Q,mean(PSNR_zbar_alpha_hat[nrepidx,:,:],dims=1)',label=latexstring("\$\\bar{z}\$ with \$\\hat{\\alpha}\$, NREP=$(NREP_arr[nrepidx])"),marker="o",linestyle="-",markersize=10,linewidth= 3,color="darkgreen")
PyPlot.plot(Q,mean(PSNR_org,dims=1)',label="org NREP=$(NREP_arr[nrepidx])",marker="o",linestyle="-",fillstyle="none",markersize=10,linewidth= 4,color="black")
PyPlot.plot(Q,mean(PSNR_zbar_alpha_hat_prec[nrepidx,:,:],dims=1)',label=latexstring("\$\\bar{z}\$ with \$\\hat{\\alpha}\$ prec, NREP=$(NREP_arr[nrepidx])"),marker="o",linestyle="-",markersize=10,linewidth= 3,color="darkgreen")

xscale("log")
yscale("log")

xlabel("q")
ylabel("PSNR")
title("PSNR vs q")
legend()

## Approximation vs NREP
# qidx=1
# figure()
# plot(NREP_arr,mean(err_xbar[:,:,qidx],dims=2),label="org q=10^$(log10(Q[qidx]))",marker="o",linestyle="--",fillstyle="none",markersize=10,linewidth= 4,color="lime")
# aidx = 1; plot(NREP_arr,mean(err_zbar_alpha[:,:,qidx,aidx],dims=2),label=latexstring("\$\\bar{z}\$ with α =$(round(alphas(Q[qidx])[aidx],sigdigits=2)), q=10^$(log10(Q[qidx]))"),marker="^",linestyle="-",markersize=10,linewidth= 3)
# aidx += 1; plot(NREP_arr,mean(err_zbar_alpha[:,:,qidx,aidx],dims=2),label=latexstring("\$\\bar{z}\$ with α =$(round(alphas(Q[qidx])[aidx],sigdigits=2)), q=10^$(log10(Q[qidx]))"),marker="v",linestyle="-",markersize=10,linewidth= 3)
# plot(NREP_arr,mean(err_zbar_alpha_hat[:,:,qidx],dims=2),label=latexstring("\$\\bar{z}\$ with \$\\hat{\\alpha}\$, q=10^$(log10(Q[qidx]))"),marker="x",linestyle="-",markersize=10,linewidth= 3,color="darkgreen")
# plot(NREP_arr,mean(err_zbar_alpha_hat_prec[:,:,qidx],dims=2),label=latexstring("\$\\bar{z}\$ with \$\\hat{\\alpha}\$ prec, q=10^$(log10(Q[qidx]))"),marker="o",linestyle="-",markersize=10,linewidth= 3,color="darkgreen")
#
# title("xbar vs zbar")
# xscale("log")
# yscale("log")
# legend()
#
# qidx=3
# figure()
# plot(NREP_arr,mean(err_xbar[:,:,qidx],dims=2),label="org q=10^$(log10(Q[qidx]))",marker="o",linestyle="--",fillstyle="none",markersize=10,linewidth= 4,color="lime")
# aidx = 1; plot(NREP_arr,mean(err_zbar_alpha[:,:,qidx,aidx],dims=2),label=latexstring("\$\\bar{z}\$ with α =$(round(alphas(Q[qidx])[aidx],sigdigits=2)), q=10^$(log10(Q[qidx]))"),marker="^",linestyle="-",markersize=10,linewidth= 3)
# aidx += 1; plot(NREP_arr,mean(err_zbar_alpha[:,:,qidx,aidx],dims=2),label=latexstring("\$\\bar{z}\$ with α =$(round(alphas(Q[qidx])[aidx],sigdigits=2)), q=10^$(log10(Q[qidx]))"),marker="v",linestyle="-",markersize=10,linewidth= 3)
# plot(NREP_arr,mean(err_zbar_alpha_hat[:,:,qidx],dims=2),label=latexstring("\$\\bar{z}\$ with \$\\hat{\\alpha}\$, q=10^$(log10(Q[qidx]))"),marker="x",linestyle="-",markersize=10,linewidth= 3,color="darkgreen")
# plot(NREP_arr,mean(err_zbar_alpha_hat_prec[:,:,qidx],dims=2),label=latexstring("\$\\bar{z}\$ with \$\\hat{\\alpha}\$ prec, q=10^$(log10(Q[qidx]))"),marker="o",linestyle="-",markersize=10,linewidth= 3,color="darkgreen")
#
# title("xbar vs zbar")
# xscale("log")
# yscale("log")
# legend()
#
# qidx = 5
# figure()
# plot(NREP_arr,mean(err_xbar[:,:,qidx],dims=2),label="org q=10^$(log10(Q[qidx]))",marker="o",linestyle="--",fillstyle="none",markersize=10,linewidth= 4,color="lime")
# aidx = 1; plot(NREP_arr,mean(err_zbar_alpha[:,:,qidx,aidx],dims=2),label=latexstring("\$\\bar{z}\$ with α =$(round(alphas(Q[qidx])[aidx],sigdigits=2)), q=10^$(log10(Q[qidx]))"),marker="^",linestyle="-",markersize=10,linewidth= 3)
# aidx += 1; plot(NREP_arr,mean(err_zbar_alpha[:,:,qidx,aidx],dims=2),label=latexstring("\$\\bar{z}\$ with α =$(round(alphas(Q[qidx])[aidx],sigdigits=2)), q=10^$(log10(Q[qidx]))"),marker="v",linestyle="-",markersize=10,linewidth= 3)
# plot(NREP_arr,mean(err_zbar_alpha_hat[:,:,qidx],dims=2),label=latexstring("\$\\bar{z}\$ with \$\\hat{\\alpha}\$, q=10^$(log10(Q[qidx]))"),marker="x",linestyle="-",markersize=10,linewidth= 3,color="darkgreen")
# plot(NREP_arr,mean(err_zbar_alpha_hat_prec[:,:,qidx],dims=2),label=latexstring("\$\\bar{z}\$ with \$\\hat{\\alpha}\$ prec, q=10^$(log10(Q[qidx]))"),marker="o",linestyle="-",markersize=10,linewidth= 3,color="darkgreen")
#
# title("xbar vs zbar")
# xscale("log")
# yscale("log")
# legend()
