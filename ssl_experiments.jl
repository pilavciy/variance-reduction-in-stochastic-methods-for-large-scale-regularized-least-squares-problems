using RandomForests
using LightGraphs
using PyPlot
using LinearAlgebra
using Random
using GraphPlot
using JLD2
using SparseArrays
using StatsBase
using LaTeXStrings
import StatsBase.denserank,Statistics.mean,Base.show,Base.sum,
StatsBase.counts,StatsBase.std

pygui(true)
include("./ssl_data_loaders.jl")
include("./utils.jl")
PyPlot.close("all")

## Choose a graph
gname = "cora"
if gname == "citeseer"
    G,gr_truth = load_citeseer()
elseif gname == "cora"
    G,gr_truth = load_cora()
elseif gname == "pubmed"
    G,gr_truth = load_PubMed()
end
encoded_labels =  encode_labels(gr_truth,maximum(gr_truth))

L = laplacian_matrix(G)
n = nv(G)

@assert is_connected(G)
# Experiment params.
Q = 10 .^ (-1.0 :0.5:1.0)
NREP_arr = [50] # Int64.(round.(10 .^ (2:2)))
M = Int64.(round.(10:10:30))
EXPREP = 100
d = degree(G)
# alphas = [/(q+maximum(degree(G)));q/(q+mean(degree(G)))]
acc(pred,gr_tr) =  sum(pred .== gr_tr)/length(gr_tr)
acc_exact = zeros(EXPREP,length(M))
acc_xbar = zeros(length(NREP_arr),EXPREP,length(M))
acc_zbar_alpha = zeros(length(NREP_arr),EXPREP,length(M),2)
acc_zbar_alpha_hat = zeros(length(NREP_arr),EXPREP,length(M))
acc_zbar_alpha_hat_pw = zeros(length(NREP_arr),EXPREP,length(M))
acc_zbar_alpha_hat_prec = zeros(length(NREP_arr),EXPREP,length(M))
alphas(q) = [2*q/(4+q)]

# Function for constant alpha
μ = 1.0
for exp_rep = 1 : EXPREP
    for (midx,m) in enumerate(M)

        Y,rootset = sample_labels(encoded_labels,m)
        unlabeled_set = setdiff(1:nv(G),rootset)
        exact = gSSL(G,μ,Y;method="exact")
        acc_exact[exp_rep,midx] = acc(gr_truth[unlabeled_set],exact[unlabeled_set])
        # alphas = [0.0001]#2*μ /(μ +4)]
        for (idx,nrep) in enumerate(NREP_arr)

            est_xbar = gSSL(G,μ,Y;nrep=nrep,method="xbar")
            acc_xbar[idx,exp_rep,midx] = acc(gr_truth[unlabeled_set],est_xbar[unlabeled_set])

            for (aidx,α) in enumerate(alphas(μ))
                est_alpha_c = gSSL(G,μ,Y;α=α,nrep=nrep,method="zbar")
                acc_zbar_alpha[idx,exp_rep,midx,aidx] = acc(gr_truth[unlabeled_set],est_alpha_c[unlabeled_set])
            end

            (est_alpha_hat) = gSSL(G,μ,Y;nrep=nrep,method="zbar_alphahat")
            acc_zbar_alpha_hat[idx,exp_rep,midx] = acc(gr_truth[unlabeled_set],est_alpha_hat[unlabeled_set])

            display("exp_rep:$exp_rep,m:$m,nrep:$nrep")
        end
    end
end
outputfolder = string("./results/",gname)
JLD2.@save string(outputfolder,"/workspace.jld2") G acc_exact acc_xbar acc_zbar_alpha acc_zbar_alpha_hat acc_zbar_alpha_hat_prec M NREP_arr EXPREP
#
