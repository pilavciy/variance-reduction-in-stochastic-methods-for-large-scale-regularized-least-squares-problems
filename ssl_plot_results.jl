using PyPlot
using JLD2
using LinearAlgebra
using LightGraphs,Statistics, Distances

pygui(false)
gname="cora"
outputfolder = string("./results/",gname)
JLD2.@load string(outputfolder,"/workspace.jld2") G acc_exact acc_xbar acc_zbar_alpha acc_zbar_alpha_hat  M NREP_arr EXPREP

f=figure(figsize= [8,3.2])

PyPlot.subplot(1,2,1)
nrep_idx = 1
plot(M,mean(acc_exact[:,:],dims=1)[:],marker="o",linestyle="-",fillstyle="none",markersize=10,linewidth= 4,color="black")
plot(M,mean(acc_xbar[nrep_idx,:,:],dims=1)[:],marker="o",linestyle="--",fillstyle="none",markersize=10,linewidth= 4,color="lime")
aidx = 1; plot(M,mean(acc_zbar_alpha[nrep_idx,:,:,aidx],dims=1)[:],marker="^",linestyle="-",markersize=10,linewidth= 3)
plot(M,mean(acc_zbar_alpha_hat[nrep_idx,:,:],dims=1)[:],marker="x",linestyle="-",markersize=10,linewidth= 3)

title(gname,fontsize=15)
xlabel("m",fontsize=13)
ylabel("Accuracy",fontsize=13)


PyPlot.subplot(1,2,2)
gname="citeseer"
outputfolder = string("./results/",gname)
JLD2.@load string(outputfolder,"/workspace.jld2") G acc_exact acc_xbar acc_zbar_alpha acc_zbar_alpha_hat  M NREP_arr EXPREP


nrep_idx = 1
plot(M,mean(acc_exact[:,:],dims=1)[:],label=latexstring("\$\\hat{x}\$"),marker="o",linestyle="-",fillstyle="none",markersize=10,linewidth= 4,color="black")
plot(M,mean(acc_xbar[nrep_idx,:,:],dims=1)[:],label=latexstring("\$\\bar{x}\$"),marker="o",linestyle="--",fillstyle="none",markersize=10,linewidth= 4,color="lime")
aidx = 1; plot(M,mean(acc_zbar_alpha[nrep_idx,:,:,aidx],dims=1)[:],label=latexstring("\$\\bar{z}\$ with fixed α"),marker="^",linestyle="-",markersize=10,linewidth= 3)
plot(M,mean(acc_zbar_alpha_hat[nrep_idx,:,:],dims=1)[:],label=latexstring("\$\\bar{z}\$ with \$\\hat{\\alpha}\$"),marker="x",linestyle="-",markersize=10,linewidth= 3)

title(gname,fontsize=15)
xlabel("m",fontsize=13)

figlegend(loc="upper center",ncol = 4,fontsize=13, bbox_to_anchor=(0.52, 0.0),framealpha=1.0)
savefig(string("./results/sslfig.pdf"),bbox_inches = "tight")
