using KrylovKit
using RandomForests
using LightGraphs
using PyPlot
using LinearAlgebra
using Random
using GraphPlot
using JLD2
using SparseArrays
using StatsBase
using LaTeXStrings
import StatsBase.denserank,Statistics.mean,Base.show,Base.sum,
StatsBase.counts,StatsBase.std

pygui(true)
include("./ssl_data_loaders.jl")
include("./utils.jl")
## Define a graph
n = 100
gname = "cora"
if gname == "citeseer"
    G,_ = load_citeseer()
elseif gname == "cora"
    G,_ = load_cora()
elseif gname == "pubmed"
    G,_ = load_PubMed()
elseif gname == "grid"
    nsqrt = Int64(round(sqrt(n)))
    G = LightGraphs.grid([nsqrt,nsqrt],periodic=true)
elseif gname == "erdos_renyi"
    davg = 10
    p = davg * n
    G = LightGraphs.erdos_renyi(n,p)
end

L = laplacian_matrix(G)
n = nv(G)
# Generate k-bandlimited signal
k = 5
SNR = 1.0
signorm = 1.0
x,y =  noisy_kband_signal_generator(L,k,SNR,signorm)

@assert is_connected(G)
# Experiment params.
Q = 10 .^ (-1.0 :0.5:1.0)
NREP_arr = Int64.(round.(10 .^ (1:0.5:3)))
EXPREP = 20
d = degree(G)
alphas(q) = [2*q/(4+q)]
err_org = zeros(length(NREP_arr),EXPREP,length(Q))
err_zbar_alpha = zeros(length(NREP_arr),EXPREP,length(Q),5)
err_zbar_alpha_hat = zeros(length(NREP_arr),EXPREP,length(Q))
err_zbar_alpha_hat_prec = zeros(length(NREP_arr),EXPREP,length(Q))

# Function for constant alpha

for qidx = 1 : length(Q)
    q = Q[qidx]*degree(G)./2
    Qinv = inv(Diagonal(q))
    exact = Matrix(Qinv*L + I)\y
    for exp_rep = 1 : EXPREP
        for (idx,nrep) in enumerate(NREP_arr)

            est_org = smooth_rf(G,q,y;nrep=nrep,variant=2).est
            err_org[idx,exp_rep,qidx] = norm(est_org - exact)^2

            for (aidx,α) in enumerate(alphas(Q[qidx]))
                est_alpha_c = smooth_rf_CV(G,q,y,α,nrep=nrep)
                err_zbar_alpha[idx,exp_rep,qidx,aidx] = norm(est_alpha_c - exact)^2
            end
            (est_alpha_hat,alpha_hat) = smooth_rf_CV(G,q,y;nrep=nrep)
            err_zbar_alpha_hat[idx,exp_rep,qidx] = norm(est_alpha_hat - exact)^2

            display("exp_rep:$exp_rep,nrep:$nrep,qidx:$qidx,alpha_hat:$alpha_hat,$(alphas(Q[qidx]))")# q:$(round.(q,sigdigits=2)),alpha_hat:$(round(alpha_hat[end],sigdigits=2)),alpha_hat_prec:$(round(alpha_hat_prec[end],sigdigits=2))")

        end
    end
end

# outputfolder = string("./results/",gname)
# JLD2.@save string(outputfolder,"/workspace.jld2") G err_org err_zbar_alpha err_zbar_alpha_hat err_zbar_alpha_hat_prec Q NREP_arr EXPREP
# JLD2.@load string(outputfolder,"/workspace.jld2") G err_org err_zbar_alpha err_zbar_alpha_hat Q NREP_arr EXPREP

figure()

qidx = 1
plot(NREP_arr,mean(err_org[:,:,qidx],dims=2),label="org q=10^$(log10(Q[qidx]))",marker="o",linestyle="--",fillstyle="none",markersize=10,linewidth= 4,color="lime")
aidx = 1; plot(NREP_arr,mean(err_zbar_alpha[:,:,qidx,aidx],dims=2),label=latexstring("\$\\bar{z}\$ with α =$(round(alphas(Q[qidx])[aidx],sigdigits=2)), q=10^$(log10(Q[qidx]))"),marker="^",linestyle="-",markersize=10,linewidth= 3)
# aidx += 1; plot(NREP_arr,mean(err_zbar_alpha[:,:,qidx,aidx],dims=2),label=latexstring("\$\\bar{z}\$ with α =$(round(alphas(Q[qidx])[aidx],sigdigits=2)), q=10^$(log10(Q[qidx]))"),marker="v",linestyle="-",markersize=10,linewidth= 3)
plot(NREP_arr,mean(err_zbar_alpha_hat[:,:,qidx],dims=2),label=latexstring("\$\\bar{z}\$ with \$\\hat{\\alpha}\$, q=10^$(log10(Q[qidx]))"),marker="x",linestyle="-",markersize=10,linewidth= 3,color="darkgreen")
# plot(NREP_arr,mean(err_zbar_alpha_hat_prec[:,:,qidx],dims=2),label=latexstring("\$\\bar{z}\$ with \$\\hat{\\alpha}\$ prec, q=10^$(log10(Q[qidx]))"),marker="o",linestyle="-",markersize=10,linewidth= 3,color="darkgreen")


title("xbar vs zbar")
xscale("log")
yscale("log")
legend()

figure()

qidx = 3
plot(NREP_arr,mean(err_org[:,:,qidx],dims=2),label="org q=10^$(log10(Q[qidx]))",marker="o",linestyle="--",fillstyle="none",markersize=10,linewidth= 4,color="lime")
aidx = 1; plot(NREP_arr,mean(err_zbar_alpha[:,:,qidx,aidx],dims=2),label=latexstring("\$\\bar{z}\$ with α =$(round(alphas(Q[qidx])[aidx],sigdigits=2)), q=10^$(log10(Q[qidx]))"),marker="^",linestyle="-",markersize=10,linewidth= 3)
# aidx += 1; plot(NREP_arr,mean(err_zbar_alpha[:,:,qidx,aidx],dims=2),label=latexstring("\$\\bar{z}\$ with α =$(round(alphas(Q[qidx])[aidx],sigdigits=2)), q=10^$(log10(Q[qidx]))"),marker="v",linestyle="-",markersize=10,linewidth= 3)
plot(NREP_arr,mean(err_zbar_alpha_hat[:,:,qidx],dims=2),label=latexstring("\$\\bar{z}\$ with \$\\hat{\\alpha}\$, q=10^$(log10(Q[qidx]))"),marker="x",linestyle="-",markersize=10,linewidth= 3,color="darkgreen")
# plot(NREP_arr,mean(err_zbar_alpha_hat_prec[:,:,qidx],dims=2),label=latexstring("\$\\bar{z}\$ with \$\\hat{\\alpha}\$ prec, q=10^$(log10(Q[qidx]))"),marker="o",linestyle="-",markersize=10,linewidth= 3,color="darkgreen")

title("xbar vs zbar")
xscale("log")
yscale("log")
legend()


figure()

qidx = 5
plot(NREP_arr,mean(err_org[:,:,qidx],dims=2),label="org q=10^$(log10(Q[qidx]))",marker="o",linestyle="--",fillstyle="none",markersize=10,linewidth= 4,color="lime")
aidx = 1; plot(NREP_arr,mean(err_zbar_alpha[:,:,qidx,aidx],dims=2),label=latexstring("\$\\bar{z}\$ with α =$(round(alphas(Q[qidx])[aidx],sigdigits=2)), q=10^$(log10(Q[qidx]))"),marker="^",linestyle="-",markersize=10,linewidth= 3)
# aidx += 1; plot(NREP_arr,mean(err_zbar_alpha[:,:,qidx,aidx],dims=2),label=latexstring("\$\\bar{z}\$ with α =$(round(alphas(Q[qidx])[aidx],sigdigits=2)), q=10^$(log10(Q[qidx]))"),marker="v",linestyle="-",markersize=10,linewidth= 3)
plot(NREP_arr,mean(err_zbar_alpha_hat[:,:,qidx],dims=2),label=latexstring("\$\\bar{z}\$ with \$\\hat{\\alpha}\$, q=10^$(log10(Q[qidx]))"),marker="x",linestyle="-",markersize=10,linewidth= 3,color="darkgreen")
# plot(NREP_arr,mean(err_zbar_alpha_hat_prec[:,:,qidx],dims=2),label=latexstring("\$\\bar{z}\$ with \$\\hat{\\alpha}\$ prec, q=10^$(log10(Q[qidx]))"),marker="o",linestyle="-",markersize=10,linewidth= 3,color="darkgreen")

title("xbar vs zbar")
xscale("log")
yscale("log")
legend()
