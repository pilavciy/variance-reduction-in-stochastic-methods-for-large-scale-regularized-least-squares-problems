# VARIANCE REDUCTION IN STOCHASTIC METHODS FOR LARGE-SCALE REGULARIZED LEAST-SQUARES PROBLEMS

The codes for reproducing the results in:

Pilavcı, Y., Amblard, P. O., Barthelmé, S., & Tremblay, N. (2021). Variance reduction in stochastic methods for large-scale regularised least-squares problems. arXiv preprint arXiv:2110.07894.
