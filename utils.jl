# Fixed alpha with vector q
function smooth_rf_CV(g :: AbstractGraph,q::Array{Float64},Y::Array, α::Float64;nrep=10,P=I)
    Xest = zeros(size(Y));
    Zest = zeros(size(Y));
    L = laplacian_matrix(g)
    Qinv = Diagonal(q.^(-1))
    for indr in Base.OneTo(nrep)
        Xest += smooth_rf(g,q,Y;variant = 2,nrep =1).est
    end
    Xest ./= nrep
    Zest = Xest - α*P*((Qinv*L+I)*Xest - Y)
    Zest
end
# Alpha hat with vector q
function smooth_rf_CV(g :: AbstractGraph,q::Array{Float64},Y::Array;nrep=10,pointwise_alpha=false,P=I)
    Xest = zeros(size(Y));
    Yest = zeros(size(Y));
    Zest = zeros(size(Y));
    L = laplacian_matrix(g)
    Qinv = Diagonal(q.^(-1))

    alpha_num_dotprod = zeros(size(Y));
    alpha_num_ysum = zeros(size(Y));
    alpha_den = zeros(size(Y));
    Update_r = zeros(size(Y));
    Update = zeros(size(Y));
    for indr in Base.OneTo(nrep)
        X_r = smooth_rf(g,q,Y;variant = 2,nrep =1).est
        Y_r = (Qinv*L+I)*X_r
        Update_r = (Y_r .- Y)

        Xest += X_r
        alpha_num_dotprod += X_r .* Y_r
        alpha_num_ysum += Y_r
        alpha_den += Update_r.^2
        Update += Update_r
    end
    Xest ./= nrep
    Update ./= nrep
    if(pointwise_alpha)
        alpha_hat_pw = (alpha_num_dotprod - Xest.*alpha_num_ysum) ./ alpha_den
        Zest = Xest - alpha_hat_pw.*Update
    else
        alpha_hat = (sum(alpha_num_dotprod - Xest.*alpha_num_ysum,dims=1) ./ sum(alpha_den,dims=1))
        Zest = Xest - alpha_hat.*Update
    end
    (Zest,alpha_hat)
end



# Fixed alpha
function smooth_rf_CV(g :: AbstractGraph,q :: Float64,Y::Array{Float64,1}, α::Float64;nrep=10,P=I)
    Zest = zeros(size(Y));
    L = laplacian_matrix(g)
    nr = 0;
    for indr in Base.OneTo(nrep)
        rf = random_forest(g,q)
        X_r = RandomForests.Partition(rf)*Y
        Zest += X_r - α*P*((q*I+L)*X_r./q - Y)
    end
    Zest./nrep
end

# Alpha hat
function smooth_rf_CV(g :: AbstractGraph,q :: Float64,Y::Array{Float64,1};nrep=10,pointwise_alpha=false,P=I)
    Xest = zeros(size(Y));
    Yest = zeros(size(Y));
    Zest = zeros(size(Y));
    alpha_num_dotprod = zeros(size(Y));
    alpha_num_ysum = zeros(size(Y));
    alpha_den = zeros(size(Y));
    Update_r = zeros(size(Y));
    Update = zeros(size(Y));
    L = laplacian_matrix(g)
    nr = 0;
    for indr in Base.OneTo(nrep)
        rf = random_forest(g,q)
        nr += rf.nroots
        X_r = RandomForests.Partition(rf)*Y
        Y_r = P*(q*I+L)*X_r./q
        Update_r = (Y_r .- P*Y)

        Xest += X_r
        alpha_num_dotprod += X_r .* Y_r
        alpha_num_ysum += Y_r
        alpha_den += Update_r.^2
        Update += Update_r
    end
    Xest ./= nrep
    Update ./= nrep
    if(pointwise_alpha)
        alpha_hat_pw = (alpha_num_dotprod - Xest.*alpha_num_ysum) ./ alpha_den
        Zest = Xest - alpha_hat_pw.*Update
    else
        alpha_hat = (sum(alpha_num_dotprod - Xest.*alpha_num_ysum,dims=1) ./ sum(alpha_den,dims=1))
        Zest = Xest - alpha_hat.*Update
    end
    (Zest,alpha_hat)
end

function trace_CV(g::AbstractGraph,q::Float64;nrep=10,α=0.01)
    Sest = 0;
    Nhat = 0;
    Zest = 0;
    n = nv(G)
    nr = 0;
    alpha_num_dotprod = 0
    alpha_den = 0
    Update = 0
    for indr in Base.OneTo(nrep)
        rf = random_forest(g,q)
        p = Partition(rf)
        Sest_r = rf.nroots
        Nhat_r = rf.nroots
        for e in edges(g)
            if(rf.root[e.src] != rf.root[e.dst])
                Nhat_r += ((1/(p.sizep[p.part[e.src]]) + 1/(p.sizep[p.part[e.dst]]))/q)
            end
        end
        Update_r = (Nhat_r .- n )
        Zest += Sest_r - α*Update_r
    end
    Sest /= nrep
end
function trace_CV(g::AbstractGraph,q::Float64;nrep=10)
    Sest = 0;
    Nhat = 0;
    n = nv(G)
    nr = 0;
    alpha_num_dotprod = 0
    alpha_den = 0
    Update = 0
    for indr in Base.OneTo(nrep)
        rf = random_forest(g,q)
        p = Partition(rf)
        Sest_r = rf.nroots
        Nhat_r = rf.nroots
        for e in edges(g)
            if(rf.root[e.src] != rf.root[e.dst])
                Nhat_r += ((1/(p.sizep[p.part[e.src]]) + 1/(p.sizep[p.part[e.dst]]))/q)
            end
        end
        Sest += Sest_r
        Nhat += Nhat_r

        alpha_num_dotprod += Sest_r * Nhat_r
        Update_r = (Nhat_r .- n )
        alpha_den += Update_r^2
        Update += Update_r
    end

    Sest /= nrep
    alpha_hat = (alpha_num_dotprod - Sest*Nhat)/alpha_den
    Update /= nrep
    Zest = Sest - alpha_hat*Update
    (Zest,alpha_hat)
end

function sample_labels(labels,m)
    numofclass = size(labels,2)
    n = size(labels,1)
    Y = zeros(n,numofclass)
    labeled_nodes = []
    for i = 1 : numofclass
        labeled_nodes_per_class = StatsBase.sample(findall(labels[:,i] .== 1), m; replace=false)
        Y[labeled_nodes_per_class,i] .= 1
        labeled_nodes = vcat(labeled_nodes,labeled_nodes_per_class)
    end
    return Y,labeled_nodes
end

function encode_labels(labels,numofclass)
    classlist = unique(labels)
    Y = zeros(length(labels),numofclass)
    for i in classlist
        labeled_nodes_per_class = findall(labels .== i)
        Y[labeled_nodes_per_class,i] .= 1
    end
    return Y
end

# Fixed alpha
function gSSL(G,μ,Y;sigma=0.0,method="exact" ,nrep=10,α=0.01)
    d = reshape(degree(G),nv(G))
    q = (μ/2) .* d

    D_sm1 = spdiagm(0 => d.^(sigma-1))
    D_1ms = spdiagm(0 => d.^(1-sigma))

    Z = D_sm1 * Y
    if (method=="exact")
        S = D_1ms*smooth(G,q,Z)
    else
        Zbar = repeat(mean(Z, dims=1), outer=size(Z,1))
        Δ = Z - Zbar
        if (method=="xbar")
            S =  D_1ms*(Zbar + smooth_rf(G,q,Δ,[];nrep=nrep,variant=2).est)
        elseif (method=="zbar")
            S =  D_1ms*(Zbar + smooth_rf_CV(G,q,Δ,α;nrep=nrep))
        elseif (method=="zbar_alphahat")
            S =  D_1ms*(Zbar + smooth_rf_CV(G,q,Δ;nrep=nrep)[1])
        end
    end
    cluster_ind = zeros(Int64, nv(G))
    for i=1:nv(G)
        cluster_ind[i] = argmax(S[i,:])
    end
    return cluster_ind
end

function noisy_kband_signal_generator(L,k::Int64,SNR::Float64,signorm::Float64)
    N = size(L,1)
    v, Uk, info = KrylovKit.eigsolve(L, k, :SR, krylovdim = max(KrylovDefaults.krylovdim, 2*k))
    Uk = reduce(hcat, Uk[1:k])
    ## create a signal to filter
    x_fourier = rand(k)
    x = Uk*x_fourier
    x = signorm*x ./ norm(x)
    σ =  signorm / sqrt(N * SNR)
    x,x + randn(N)*σ
end
