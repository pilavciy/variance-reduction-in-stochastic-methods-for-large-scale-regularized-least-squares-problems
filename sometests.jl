using RandomForests
using LightGraphs
using PyPlot
using LinearAlgebra
using Random
using GraphPlot
using JLD2
using SparseArrays
using StatsBase
using LaTeXStrings
import StatsBase.denserank,Statistics.mean,Base.show,Base.sum,
StatsBase.counts,StatsBase.std

pygui(true)
include("./ssl_data_loaders.jl")
include("./utils.jl")
PyPlot.close("all")

## Choose a graph
gname = "cora"
if gname == "citeseer"
    G,gr_truth = load_citeseer()
elseif gname == "cora"
    G,gr_truth = load_cora()
elseif gname == "pubmed"
    G,gr_truth = load_PubMed()
end
encoded_labels =  encode_labels(gr_truth,maximum(gr_truth))
# Check encoding
labels = zeros(length(gr_truth))
for i = 1 : length(gr_truth)
    labels[i] = argmax(encoded_labels[i,:])
end
@assert all(labels .== gr_truth)

acc(pred,gr_tr) =  sum(pred .== gr_tr)/length(gr_tr)

L = laplacian_matrix(G)
n = nv(G)
m = 30

Y,rootset = sample_labels(encoded_labels,m)
@assert(sum(Y) == maximum(gr_truth)*m)
unlabeled_set = setdiff(1:nv(G),rootset)

## TEST
L = Matrix(laplacian_matrix(G))
μ = 1.0
α = 2*μ /(μ +4)
d = reshape(degree(G),nv(G))
q = (μ/2) .* d
sigma = 0.0
nrep = 1000
D_sm1 = spdiagm(0 => d.^(sigma-1))
D_1ms = spdiagm(0 => d.^(1-sigma))

S = D_1ms*smooth(G,q, D_sm1*Y)
Zbar = D_1ms*smooth_rf_CV(G,q, D_sm1*Y,α;nrep=nrep)
Zbar_class = zeros(Int64, nv(G))
for i=1:nv(G)
    Zbar_class[i] = argmax(Zbar[i,:])
end

Zbar_hat,alphahat = smooth_rf_CV(G,q, D_sm1*Y;nrep=nrep)
Zbar_hat = D_1ms*Zbar_hat
display("alpha:$α, alphahat:$alphahat")
Zbarhat_class = zeros(Int64, nv(G))
for i=1:nv(G)
    Zbarhat_class[i] = argmax(Zbar_hat[i,:])
end

Xbar = D_1ms*smooth_rf(G,q, D_sm1*Y;nrep=nrep,variant=2).est
Xbar_class = zeros(Int64, nv(G))
for i=1:nv(G)
    Xbar_class[i] = argmax(Xbar[i,:])
end
display(norm(S-Zbar_hat))
display(norm(S-Zbar))
display(norm(S-Xbar))
display("accZbarhat:$(acc(Zbarhat_class[unlabeled_set],gr_truth[unlabeled_set])), accZbar:$(acc(Zbar_class[unlabeled_set],gr_truth[unlabeled_set])) , accXbar:$(acc(Xbar_class[unlabeled_set],gr_truth[unlabeled_set]))")



@assert norm(S-Zbar) < norm(S-Xbar)
@assert norm(S-Zbar_hat) < norm(S-Xbar)
@assert acc(Zbar_class[unlabeled_set],gr_truth[unlabeled_set]) > acc(Xbar_class[unlabeled_set],gr_truth[unlabeled_set])
